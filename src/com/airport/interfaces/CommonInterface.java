package com.airport.interfaces;

import java.util.List;

public interface CommonInterface<T, S> {
    public T get(List<T> t,S s);
    public S delete(List<T> t,S s);
    public List<T> add(T t);
    public void getAll(List<T> t);
}
