package com.airport.interfaceImpl;

import com.airport.interfaces.CommonInterface;
import com.airport.model.CheckingAgent_DTO;
import com.airport.model.InfoAgent_DTO;

import java.util.ArrayList;
import java.util.List;

/**
 * @CheckingAgent_DAO : questa classe implementa quatro methodi per manipolare i dati degli adetti al checking.
 * Le operazioni sono ricerca adetto, cancellazione adetto, aggiunta nuovo adetto e ricerca di tutti gli adetti
 */

public class CheckingAgent_DAO implements CommonInterface<CheckingAgent_DTO,String> {
    private List<CheckingAgent_DTO>checkingagentList = new ArrayList<>();
    @Override
    public CheckingAgent_DTO get(List<CheckingAgent_DTO> t, String s) {
        CheckingAgent_DTO checkingagent = new CheckingAgent_DTO();
        t.forEach(element->{
            //first I verified if the list contains the searched element and if true I return this element find
            if(element.get_agentNumber()==s){
                checkingagent.set_agentNumber(element.get_agentNumber());
                checkingagent.set_name(element.get_name());
                checkingagent.set_surname(element.get_surname());
                checkingagent.set_address(element.get_address());
                checkingagent.set_dateOfBirth(element.get_dateOfBirth());
                checkingagent.set_gender(element.get_gender());
                checkingagent.set_socialSNumber(element.get_socialSNumber());
            }
        });

        return checkingagent;
    }

    @Override
    public String delete(List<CheckingAgent_DTO> t, String s) {

        //First I search if the element exit on the list,if it's true I delete a this element
        t.forEach(element->{
            if(element.get_agentNumber()==s){
                t.remove(element);
            }
        });

        return "InfoAgent  with number "+s+" was remove succefully";
    }

    @Override
    public List<CheckingAgent_DTO> add(CheckingAgent_DTO checkingAgent_dto) {

        checkingagentList = new ArrayList<>();
        CheckingAgent_DTO checkingAgent_dto1 = new CheckingAgent_DTO();
        //set a new checkingagent
        checkingAgent_dto1.set_agentNumber(checkingAgent_dto.get_agentNumber());
        checkingAgent_dto1.set_socialSNumber(checkingAgent_dto.get_socialSNumber());
        checkingAgent_dto1.set_gender(checkingAgent_dto.get_gender());
        checkingAgent_dto1.set_dateOfBirth(checkingAgent_dto.get_dateOfBirth());
        checkingAgent_dto1.set_address(checkingAgent_dto.get_address());
        checkingAgent_dto1.set_name(checkingAgent_dto.get_name());
        checkingAgent_dto1.set_surname(checkingAgent_dto.get_surname());


        //add a new info agent in the list
        checkingagentList.add(checkingAgent_dto1);
        return checkingagentList;
    }

    @Override
    public void getAll(List<CheckingAgent_DTO> t) {
        //use forEach to iterate and print every element of list of checkingagent
        t.forEach(element->{
            System.out.println(element.get_agentNumber());
            System.out.println(element.get_surname());
            System.out.println(element.get_name());
            System.out.println(element.get_address());
            System.out.println(element.get_dateOfBirth());
            System.out.println(element.get_gender());
        });
    }
}
