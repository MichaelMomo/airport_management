package com.airport.interfaceImpl;

import com.airport.interfaces.CommonInterface;
import com.airport.model.City_DTO;
import com.airport.model.Fly_DTO;

import java.util.ArrayList;
import java.util.List;

/**
 * @City_DAO : questa classe implementa quatro methodi per manipolare i dati della città.
 * Le operazioni sono ricerca città, cancellazione città, aggiunta nuova città e ricerca di tutte le città.
 */

public class City_DAO implements CommonInterface<City_DTO,String> {
    private List<City_DTO> city;
    @Override
    public City_DTO get(List<City_DTO> t, String s) {
        City_DTO city_dto = new City_DTO();
        t.forEach(element->{
            //first I verified if the list contains the searched element and if true I return this element find
            if(element.get_cityCode()==s){
                city_dto.set_cityCode(element.get_cityCode());
                city_dto.set_cityCountry(element.get_cityCountry());
                city_dto.set_cityName(element.get_cityName());
            }
        });

        return city_dto;
    }

    @Override
    public String delete(List<City_DTO> t, String s) {

        //First I search if the element exit on the list,if it's true I delete a this element
        t.forEach(element->{
            if(element.get_cityCode()==s){
                t.remove(element);
            }
        });

        return "City with number "+s+" was remove succefully";
    }

    @Override
    public List<City_DTO> add(City_DTO city_dto) {
       city = new ArrayList<>();
       City_DTO city_dto1 = new City_DTO();

       // set new airplane
       city_dto1.set_cityCode(city_dto.get_cityCode());
       city_dto1.set_cityCountry(city_dto.get_cityCountry());
       city_dto1.set_cityCode(city_dto.get_cityCode());
       //add a new object city in the arraylist
       city.add(city_dto1);
        return city;
    }

    @Override
    public void getAll(List<City_DTO> t) {

        //use forEach to iterate and print every element of list of city
        t.forEach(element->{
            System.out.println(element.get_cityCode());
            System.out.println(element.get_cityName());
            System.out.println(element.get_cityCountry());
        });
    }
}
