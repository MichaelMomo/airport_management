package com.airport.interfaceImpl;

import com.airport.interfaces.CommonInterface;
import com.airport.model.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @Passenger_DAO : questa classe implementa quatro methodi per manipolare i dati del passegero.
 * Le operazioni sono ricerca passegero, cancellazione passegero, aggiunta nuovo passegero
 * e ricerca di tutti i passegeri.
 */

public class Passenger_DAO implements CommonInterface<Passenger_DTO,String> {
    private List<Passenger_DTO> passengerList;

    @Override
    public Passenger_DTO get(List<Passenger_DTO> t, String s) {

        Passenger_DTO passenger = new Passenger_DTO();
        t.forEach(element->{
            //first I verified if the list contains the searched element and if true I return this element find
            if(element.get_socialSNumber()==s){
                passenger.set_socialSNumber(element.get_socialSNumber());
                passenger.set_passengerPassport(element.get_passengerPassport());
                passenger.set_passengerPassport(element.get_passengerPassport());
                passenger.set_passengerSuitcase(element.get_passengerSuitcase());
                passenger.set_passengerTicket(element.get_passengerTicket());

            }
        });
        return passenger;
    }

    @Override
    public String delete(List<Passenger_DTO> t, String s) {

        //First I search if the element exit on the list,if it's true I delete a this element
        t.forEach(element->{
            if(element.get_socialSNumber()==s){
                t.remove(element);
            }
        });

        return "Passenger with SSN "+s+" was remove succefully";
    }

    @Override
    public List<Passenger_DTO> add(Passenger_DTO passenger_dto) {

        passengerList = new ArrayList<>();
        Passenger_DTO passenger_dto1 = new Passenger_DTO();
        Passport_DTO passport = new Passport_DTO();
        Ticket_DTO ticket = new Ticket_DTO();
        Suitcase_DTO suitcase = new Suitcase_DTO();

        //set a new suitcase
        suitcase.set_sCode(passenger_dto.get_passengerSuitcase().get_sCode());
        suitcase.set_sWeight(passenger_dto.get_passengerSuitcase().get_sWeight());

        //set a new passport
        passport.set_passportNumber(passenger_dto.get_passengerPassport().get_passportNumber());
        passport.set_authority(passenger_dto.get_passengerPassport().get_authority());
        passport.set_expireDate(passenger_dto.get_passengerPassport().get_expireDate());
        passport.set_deliverDate(passenger_dto.get_passengerPassport().get_deliverDate());

        //set a new ticket
        ticket.set_ticketNumber(passenger_dto.get_passengerTicket().get_ticketNumber());
        ticket.set_checkingDate(passenger_dto.get_passengerTicket().get_checkingDate());
        ticket.set_ticketCheckingDTO(passenger_dto.get_passengerTicket().get_ticketCheckingDTO());
        ticket.set_departureDate(passenger_dto.get_passengerTicket().get_departureDate());
        ticket.set_arrivalDate(passenger_dto.get_passengerTicket().get_arrivalDate());
        ticket.set_airportFrom(passenger_dto.get_passengerTicket().get_airportFrom());
        ticket.set_airportTo(passenger_dto.get_passengerTicket().get_airportTo());
        ticket.set_checkingDate(passenger_dto.get_passengerTicket().get_checkingDate());
        ticket.set_flyClass(passenger_dto.get_passengerTicket().get_flyClass());

        //set new passenger
        passenger_dto1.set_passengerPassport(passport);
        passenger_dto1.set_passengerTicket(ticket);
        passenger_dto1.set_passengerSuitcase(suitcase);
        passenger_dto1.set_surname(passenger_dto.get_surname());
        passenger_dto1.set_name(passenger_dto.get_name());
        passenger_dto1.set_address(passenger_dto.get_address());
        passenger_dto1.set_dateOfBirth(passenger_dto.get_dateOfBirth());
        passenger_dto1.set_gender(passenger_dto.get_gender());
        passenger_dto1.set_socialSNumber(passenger_dto.get_socialSNumber());

        //add a passenger in the list
        passengerList.add(passenger_dto1);
        return passengerList;

    }

    @Override
    public void getAll(List<Passenger_DTO> t) {


        //use forEach to iterate and print every element of list of Passenger
        t.forEach(element->{
            System.out.println(element.get_socialSNumber());
            System.out.println(element.get_passengerPassport().get_passportNumber());
            System.out.println(element.get_name());
            System.out.println(element.get_surname());
            System.out.println(element.get_passengerPassport().get_deliverDate());
            System.out.println(element.get_passengerPassport().get_expireDate());
            System.out.println(element.get_passengerSuitcase().get_sCode());
            System.out.println(element.get_passengerSuitcase().get_sWeight());
            System.out.println(element.get_passengerTicket().get_ticketNumber());
            System.out.println("*************************************************");
        });
    }
}
