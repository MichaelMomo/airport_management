package com.airport.interfaceImpl;

import com.airport.interfaces.CommonInterface;
import com.airport.model.AirPlane_DTO;
import com.airport.model.AirPort_DTO;
import com.airport.model.City_DTO;
import com.airport.model.FlyCompagny_DTO;

import java.util.ArrayList;
import java.util.List;

/**
 * @Fly_Compagny_DAO : questa classe implementa quatro methodi per manipolare i dati della compagnia aereana.
 * Le operazioni sono ricerca compagnia aereana, cancellazione compagnia aereana, aggiunta nuova compagnia aereana
 * e ricerca di tutte le compagnie aereana
 */

public class Fly_Compagny_DAO implements CommonInterface<FlyCompagny_DTO,String> {
    private List<FlyCompagny_DTO> flycomoagny ;
    @Override
    public FlyCompagny_DTO get(List<FlyCompagny_DTO> t, String s) {

        FlyCompagny_DTO flycomp = new FlyCompagny_DTO();
        t.forEach(element->{
            //first I verified if the list contains the searched element and if true I return this element find
            if(element.get_compagnyName()==s){
                flycomp.set_country(element.get_country());
                flycomp.set_compagnyName(element.get_compagnyName());
                flycomp.set_airplaneModel(element.get_airplaneModel());
            }
        });

        return flycomp;
    }

    @Override
    public String delete(List<FlyCompagny_DTO> t, String s) {

        //First I search if the element exit on the list,if it's true I delete a this element
        t.forEach(element->{
            if(element.get_compagnyName()==s){
                t.remove(element);
            }
        });

        return "Flycompagny with name "+s+" was remove succefully";
    }

    @Override
    public List<FlyCompagny_DTO> add(FlyCompagny_DTO flyCompagnyDTO) {


        flycomoagny = new ArrayList<>();
        FlyCompagny_DTO flycomoagny1 = new FlyCompagny_DTO();
        AirPlane_DTO  airplanemodel = new AirPlane_DTO();


        //set a new airplanemdel
        airplanemodel.set_model(flyCompagnyDTO.get_airplaneModel().get_model());
        airplanemodel.set_capacity(flyCompagnyDTO.get_airplaneModel().get_capacity());

        // set new flycompagny
        flycomoagny1.set_airplaneModel(airplanemodel);
        flycomoagny1.set_compagnyName(flyCompagnyDTO.get_compagnyName());
        flycomoagny1.set_country(flyCompagnyDTO.get_country());

        //add a new object flycompagny in the arraylist
        flycomoagny.add(flycomoagny1);
        return flycomoagny;


    }

    @Override
    public void getAll(List<FlyCompagny_DTO> t) {

        //use forEach to iterate and print every element of list of airplane
        t.forEach(element->{
            System.out.println(element.get_compagnyName());
            System.out.println(element.get_country());
            System.out.println(element.get_airplaneModel().get_model());

            System.out.println("*************************************************");
        });
    }
}
