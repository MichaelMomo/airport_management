package com.airport.interfaceImpl;

import com.airport.interfaces.CommonInterface;
import com.airport.model.InfoAgent_DTO;
import com.airport.model.TicketChecking_DTO;

import java.util.ArrayList;
import java.util.List;

/**
 * @InfoAgent_DAO : questa classe implementa quatro methodi per manipolare i dati dell'adetto alle informazioni.
 * Le operazioni sono ricerca adetto alle informazioni, cancellazione adetto alle informazioni, aggiunta nuovo adetto alle informazioni
 * e ricerca di tutti gli adetti alle informazioni.
 */

public class InfoAgent_DAO implements CommonInterface<InfoAgent_DTO,String> {
    private List<InfoAgent_DTO>  infoagentList;
    @Override
    public InfoAgent_DTO get(List<InfoAgent_DTO> t, String s) {

        InfoAgent_DTO infoagent = new InfoAgent_DTO();
        t.forEach(element->{
            //first I verified if the list contains the searched element and if true I return this element find
            if(element.get_infoAgentNumber()==s){
                infoagent.set_infoAgentNumber(element.get_infoAgentNumber());
                infoagent.set_name(element.get_name());
                infoagent.set_surname(element.get_surname());
                infoagent.set_address(element.get_address());
                infoagent.set_dateOfBirth(element.get_dateOfBirth());
                infoagent.set_gender(element.get_gender());
                infoagent.set_socialSNumber(element.get_socialSNumber());
            }
        });
        return infoagent;
    }

    @Override
    public String delete(List<InfoAgent_DTO> t, String s) {

        //First I search if the element exit on the list,if it's true I delete a this element
        t.forEach(element->{
            if(element.get_infoAgentNumber()==s){
                t.remove(element);
            }
        });

        return "InfoAgent  with number "+s+" was remove succefully";
    }

    @Override
    public List<InfoAgent_DTO> add(InfoAgent_DTO infoAgent_dto) {
        infoagentList = new ArrayList<>();
        InfoAgent_DTO infoAgent_dto1 = new InfoAgent_DTO();
        //set a new infoagent
        infoAgent_dto1.set_infoAgentNumber(infoAgent_dto.get_infoAgentNumber());
        infoAgent_dto1.set_socialSNumber(infoAgent_dto.get_socialSNumber());
        infoAgent_dto1.set_gender(infoAgent_dto.get_gender());
        infoAgent_dto1.set_dateOfBirth(infoAgent_dto.get_dateOfBirth());
        infoAgent_dto1.set_address(infoAgent_dto.get_address());
        infoAgent_dto1.set_name(infoAgent_dto.get_name());
        infoAgent_dto1.set_surname(infoAgent_dto.get_surname());


        //add a new info agent in the list
        infoagentList.add(infoAgent_dto1);
        return infoagentList;

    }

    @Override
    public void getAll(List<InfoAgent_DTO> t) {
        //use forEach to iterate and print every element of list of Infoagent
        t.forEach(element->{
            System.out.println(element.get_infoAgentNumber());
            System.out.println(element.get_surname());
            System.out.println(element.get_name());
            System.out.println(element.get_address());
            System.out.println(element.get_dateOfBirth());
            System.out.println(element.get_gender());
        });

    }
}
