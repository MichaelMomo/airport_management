package com.airport.interfaceImpl;

import com.airport.interfaces.CommonInterface;
import com.airport.model.CheckingAgent_DTO;
import com.airport.model.City_DTO;
import com.airport.model.TicketChecking_DTO;

import java.util.ArrayList;
import java.util.List;

/**
 * @TicketChecking_DTO : questa classe implementa quatro methodi per la gestione del checking.
 * Le operazioni sono ricerca checking, cancellazione checking, aggiunta nuovo checking
 * e ricerca di tutti i checking.
 */

public class TicketChecking_DAO implements CommonInterface<TicketChecking_DTO,String> {
    private List<TicketChecking_DTO> ticketChecking_dto;
    @Override
    public TicketChecking_DTO get(List<TicketChecking_DTO> t, String s) {

        TicketChecking_DTO checking_dto = new TicketChecking_DTO();
        t.forEach(element->{
            //first I verified if the list contains the searched element and if true I return this element find
            if(element.get_checkingAgent().get_agentNumber()==s){
                checking_dto.set_checkingDate(element.get_checkingDate());
                checking_dto.set_checkingAgent(element.get_checkingAgent());
            }
        });
        return checking_dto;

    }

    @Override
    public String delete(List<TicketChecking_DTO> t, String s) {
        //First I search if the element exit on the list,if it's true I delete a this element
        t.forEach(element->{
            if(element.get_checkingAgent().get_agentNumber()==s){
                t.remove(element);
            }
        });

        return "TicketChecking  with number "+s+" was remove succefully";

    }

    @Override
    public List<TicketChecking_DTO> add(TicketChecking_DTO ticketChecking_DTO) {
        ticketChecking_dto = new ArrayList<>();
        TicketChecking_DTO ticket_a = new TicketChecking_DTO();

        CheckingAgent_DTO  checkingagent = new CheckingAgent_DTO();

        //set a new checkingagent
        checkingagent.set_agentNumber(ticketChecking_DTO.get_checkingAgent().get_agentNumber());
        checkingagent.set_socialSNumber(ticketChecking_DTO.get_checkingAgent().get_socialSNumber());
        checkingagent.set_address(ticketChecking_DTO.get_checkingAgent().get_address());
        checkingagent.set_dateOfBirth(ticketChecking_DTO.get_checkingAgent().get_dateOfBirth());
        checkingagent.set_gender(ticketChecking_DTO.get_checkingAgent().get_gender());
        checkingagent.set_name(ticketChecking_DTO.get_checkingAgent().get_name());
        checkingagent.set_surname(ticketChecking_DTO.get_checkingAgent().get_surname());

        //set a new ticketchecking
        ticket_a.set_checkingDate(ticketChecking_DTO.get_checkingDate());
        ticket_a.set_checkingAgent(checkingagent);
        //add a new  ticketchecking in the list
        ticketChecking_dto.add(ticket_a);


        return ticketChecking_dto;
    }

    @Override
    public void getAll(List<TicketChecking_DTO> t) {


        //use forEach to iterate and print every element of list of city
        t.forEach(element->{
            System.out.println(element.get_checkingAgent().get_agentNumber());
            System.out.println(element.get_checkingAgent().get_name());
            System.out.println(element.get_checkingAgent().get_surname());
            /*System.out.println(element.get_checkingAgent().get_gender());
            System.out.println(element.get_checkingAgent().get_dateOfBirth());
            System.out.println(element.get_checkingAgent().get_address());*/
            System.out.println(element.get_checkingDate());

        });

    }
}
