package com.airport.model;

/**
 * @CheckingAgent_DTO
 */

public class CheckingAgent_DTO extends Person_DTO {
    private String _agentNumber;

    public CheckingAgent_DTO() {
        super();
    }

    public String get_agentNumber() {
        return _agentNumber;
    }

    public void set_agentNumber(String _agentNumber) {
        this._agentNumber = _agentNumber;
    }

    @Override
    public String toString() {
        return "CheckingAgent_DTO{" +
                "_agentNumber='" + _agentNumber + '\'' +
                '}';
    }
}
