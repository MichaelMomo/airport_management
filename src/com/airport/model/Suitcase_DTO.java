package com.airport.model;

/**
 * @public Suitcase_DTO
 */

public class Suitcase_DTO {
    private String _sCode;
    private int _sWeight;

    public String get_sCode() {
        return _sCode;
    }

    public void set_sCode(String _sCode) {
        this._sCode = _sCode;
    }

    public int get_sWeight() {
        return _sWeight;
    }

    public void set_sWeight(int _sWeight) {
        this._sWeight = _sWeight;
    }

    @Override
    public String toString() {
        return "Suitcase_DTO{" +
                "_sCode='" + _sCode + '\'' +
                ", _sWeight=" + _sWeight +
                '}';
    }
}
