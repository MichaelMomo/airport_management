package com.airport.model;

/**
 * @City_DTO
 */

public class City_DTO {
    private String _cityName;
    private String _cityCountry;
    private String _cityCode;

    public String get_cityName() {
        return _cityName;
    }

    public void set_cityName(String _cityName) {
        this._cityName = _cityName;
    }

    public String get_cityCountry() {
        return _cityCountry;
    }

    public void set_cityCountry(String _cityCountry) {
        this._cityCountry = _cityCountry;
    }

    public String get_cityCode() {
        return _cityCode;
    }

    public void set_cityCode(String _cityCode) {
        this._cityCode = _cityCode;
    }

    @Override
    public String toString() {
        return "City_DTO{" +
                "_cityName='" + _cityName + '\'' +
                ", _cityCountry='" + _cityCountry + '\'' +
                ", _cityCode='" + _cityCode + '\'' +
                '}';
    }
}
