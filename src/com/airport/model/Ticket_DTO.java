package com.airport.model;

/**
 * @Ticket_DTO
 */

public class Ticket_DTO {
    private String _ticketNumber;
    private String _departureDate;
    private String _arrivalDate;
    private String _checkingDate;
    private AirPort_DTO _airportFrom;
    private AirPort_DTO _airportTo;
    private String _flyClass;
    private TicketChecking_DTO _ticketCheckingDTO;

    public String get_ticketNumber() {
        return _ticketNumber;
    }

    public void set_ticketNumber(String _ticketNumber) {
        this._ticketNumber = _ticketNumber;
    }

    public String get_departureDate() {
        return _departureDate;
    }

    public void set_departureDate(String _departureDate) {
        this._departureDate = _departureDate;
    }

    public String get_arrivalDate() {
        return _arrivalDate;
    }

    public void set_arrivalDate(String _arrivalDate) {
        this._arrivalDate = _arrivalDate;
    }

    public String get_checkingDate() {
        return _checkingDate;
    }

    public void set_checkingDate(String _checkingDate) {
        this._checkingDate = _checkingDate;
    }

    public AirPort_DTO get_airportFrom() {
        return _airportFrom;
    }

    public void set_airportFrom(AirPort_DTO _airportFrom) {
        this._airportFrom = _airportFrom;
    }

    public AirPort_DTO get_airportTo() {
        return _airportTo;
    }

    public void set_airportTo(AirPort_DTO _airportTo) {
        this._airportTo = _airportTo;
    }

    public String get_flyClass() {
        return _flyClass;
    }

    public void set_flyClass(String _flyClass) {
        this._flyClass = _flyClass;
    }

    public TicketChecking_DTO get_ticketCheckingDTO() {
        return _ticketCheckingDTO;
    }

    public void set_ticketCheckingDTO(TicketChecking_DTO _ticketCheckingDTO) {
        this._ticketCheckingDTO = _ticketCheckingDTO;
    }

    @Override
    public String toString() {
        return "Ticket_DTO{" +
                "_ticketNumber='" + _ticketNumber + '\'' +
                ", _departureDate='" + _departureDate + '\'' +
                ", _arrivalDate='" + _arrivalDate + '\'' +
                ", _checkingDate='" + _checkingDate + '\'' +
                ", _airportFrom=" + _airportFrom +
                ", _airportTo=" + _airportTo +
                ", _flyClass='" + _flyClass + '\'' +
                ", _ticketCheckingDTO=" + _ticketCheckingDTO +
                '}';
    }
}
