package com.airport.model;

/**
 * @Passport_DTO
 */

public class Passport_DTO extends Person_DTO {
    private String _passportNumber;
    private String _deliverDate;
    private String _expireDate;
    private String _authority;

    public String get_passportNumber() {
        return _passportNumber;
    }

    public void set_passportNumber(String _passportNumber) {
        this._passportNumber = _passportNumber;
    }

    public String get_deliverDate() {
        return _deliverDate;
    }

    public void set_deliverDate(String _deliverDate) {
        this._deliverDate = _deliverDate;
    }

    public String get_expireDate() {
        return _expireDate;
    }

    public void set_expireDate(String _expireDate) {
        this._expireDate = _expireDate;
    }

    public String get_authority() {
        return _authority;
    }

    public void set_authority(String _authority) {
        this._authority = _authority;
    }

    public Passport_DTO() {
        super();
    }

    @Override
    public String toString() {
        return "Passport_DTO{" +
                "_passportNumber=" + _passportNumber +
                ", _deliverDate='" + _deliverDate + '\'' +
                ", _expireDate='" + _expireDate + '\'' +
                ", _authority='" + _authority + '\'' +
                '}';
    }
}
