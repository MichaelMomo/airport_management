package com.airport.model;

/**
 * @AirPort_DTO
 */

public class AirPort_DTO {
    private String _airportName;
    private City_DTO _airportCity;

    public String get_airportName() {
        return _airportName;
    }

    public void set_airportName(String _airportName) {
        this._airportName = _airportName;
    }

    public City_DTO get_airportCity() {
        return _airportCity;
    }

    public void set_airportCity(City_DTO _airportCity) {
        this._airportCity = _airportCity;
    }

    @Override
    public String toString() {
        return "AirPort_DTO{" +
                "_airportName='" + _airportName + '\'' +
                ", _airportCity=" + _airportCity +
                '}';
    }
}
